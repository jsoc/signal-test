#!/bin/bash

_term() { 
  echo "Caught termination signal!" 
  terminate=TRUE
}

trap _term TERM
trap _term INT

terminate=false
while [[ $terminate != "TRUE" ]]; do
    echo "Working..."
    sleep 1
done
echo "Terminated Gracefully."

